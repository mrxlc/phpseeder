# PHPSeeder #

This library is designed to make some seeds for the insertion of information in the database in a very simple way.

Example:

```
use Mrxlc\Seeder\Seeder;

$table = new Seeder();
$table->setTableName('users');

for($i = 1;$i<=100;$i++)
{
	$cells = array('Username' => 'User'.$i,'Password' => '123456' );
	$table->add($cells);
}

```

It is necessary to create the instance of the object, then add the name of the table to be filled with the following method `SetTableName($tableName)`

Then add the information in array format as follows:

`array('Column1' => 'Value1','Column2' => 'Value2' )`

Finally add the information with the following method:

`add($array)`
