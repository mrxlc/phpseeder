<?php
namespace Mrxlc\Seeder;

use Mrxlc\Connection\Connection;


class Seeder extends Connection
{
	protected $TableName;

	public function setTableName($tableName)
	{
		$this->TableName = $tableName;
	}

	public function add($cells)
	{
		$sentence = $this->getSentence($cells);
		$exe = $this->sentence("SET CHARACTER SET utf8");
		$exe = $this->sentence($sentence);
	}

	public function getSentence($cells)
	{
		$columns = array();
		$values = array();
	 	foreach ($cells as $cell => $value)
	 	{
	 		array_unshift($columns, $cell);
	 		array_unshift($values, $value);
	 	}

	 	return $this->generateInsert($columns,$values);
	}

	function generateInsert($columns,$values)
	{
		$return = 'INSERT INTO ' . $this->TableName;
		$tableColumns = '(';
		$tableValues = ' VALUES(';
		for($i = 0; $i < count($columns);$i++)
		{
			$tableColumns.= $columns[$i].',';
			if($this->needScapeQuote($values[$i]))
			{
				$tableValues.= '\''.$values[$i].'\',';
			}else
			{
				$tableValues.= $values[$i].',';
			}	
		}
		$tableColumns = substr($tableColumns, 0, -1);
		$tableValues = substr($tableValues, 0, -1);
		$tableColumns.= ")";
		$tableValues.= ")";

		return $return.$tableColumns.$tableValues;

	}

	function needScapeQuote($value)
	{
		if(is_string($value))
		{
			return true;
		}
		if(is_numeric($value))
		{
			return false;
		}
		if(is_null($value))
		{
			return false;
		}
		if(is_int($value))
		{
			return false;
		}
		if(is_float($value))
		{
			return false;
		}
		if(is_bool($value))
		{
			return false;
		}
		return true;
	}

}